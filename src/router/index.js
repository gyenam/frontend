'use strict';
import Vue from 'vue';
import Router from 'vue-router';
import HelloWorld from '@/components/HelloWorld';
import SelectBitApi from '@/components/SelectBitApi';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'HelloWorld',
      component: HelloWorld,
    },
    {
      path: '/selectBitApi',
      name: 'SelectBitApi',
      component: SelectBitApi,
    },
  ],
});
